package main

import (
	"fmt"
	"math"
)

// For an initial guess, z, compute the square root of x over 10 loops
func Sqrt(x float64) float64 {
	z := 1.0
	for i := 1; i < 10; i++ {
		fmt.Println(z)
		z -= (z*z - x) / (2*z)
	}
	return z
}

// For an initial guess, z, compute the square root of x until equal to math.Sqrt(x)
func Sqrt2(x float64) float64 {
	z := 1.0
	for ;z != math.Sqrt(x); z -= (z*z - x) / (2*z) {
		fmt.Println(z)
	}
	return z
}

func main() {
	fmt.Println(Sqrt(2))
	// fmt.Println(Sqrt2(2))
}
